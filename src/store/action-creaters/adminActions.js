import {UserActionType} from "../reducers/userReducer";
import {ref, set} from "firebase/database";
import {database} from "../../utils/firebase-config";



export const addIgrAction = (name, link, desc) => {
    return async (dispatch) => {
        dispatch({ type: UserActionType.ADMIN_USER });
        try {
            const ingridient = await get(child(ref(database), `ingridients/${name}`));
            await set(ref(database, `ingridient/${ingridient.ingridient.name}`), {
                name: ingridient.ingridient.name,
                link: ingridient.ingridient.link,
                desc: ingridient.ingridient.desc,
            });

            const ingrData = await get(child(ref(database), `ingridient/${ingridient.ingridient.name}`));
            console.log(ingrData.val());

            dispatch({ type: UserActionType.REGISTER_USER, payload: userData.val() });
        } catch (e) {
            dispatch({ type: UserActionType.ERROR_USER, payload: e.message });
        }
    }
}