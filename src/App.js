import React from "react"
import{Routes, Route, Link} from 'react-router-dom'
import Authorization from './components/Authorization';
import Registration from './components/Registration';
import Load from "./components/Load";
import Profile from "./components/Profile";
import CheckIngridients from "./components/CheckIngridients";
import AdminPage from "./components/AdminPage";

function App(){
    return(
        <>
        <Routes>
            <Route path="/" element={<Load/>}/>
            <Route path="/authorization" element={<Authorization/>}/>
            <Route path="/registration" element={<Registration/>}/>
            <Route path="/profile" element={<Profile/>}/>
            <Route path="/checkingridients" element={<CheckIngridients/>}/>
            <Route path="/adminPage" element={<AdminPage/>}/>
        </Routes>

        </>
    );
}
export default App;
