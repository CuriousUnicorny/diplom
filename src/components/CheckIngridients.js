import React, { useEffect, useRef } from "react";
import { Link, Route, Routes, useNavigate } from "react-router-dom";
import Registration from "./Registration";
import Profile from "./Profile";
import { useState } from "react";
import "../styles/Style.css";
import Tesseract from "tesseract.js";

function CheckIngridients() {
    const [text, setText] = useState("");
    const [progress, setProgress] = useState(0);
    const [status, setStatus] = useState("");
    const [done, setDone] = useState(false);
    const [process, setProcess] = useState(false);

    // Распознавание изображения
    function recognize(file, lang, logger) {
        Tesseract.recognize(file, lang, { logger }).then(
            ({ data: { text } }) => {
                setText(text);
            }
        );
    }

    // Отслеживание прогресса обработки
    function updateProgress(data) {
        setProgress(data.progress);
        setStatus(data.status);
        if (data.progress === 1 && data.status === "recognizing text") {
            setDone(!done);
        }
    }

    // Вывод результата
    function setResult(text) {
        let textNew = text.replace(/\n\s*\n/g, "\n");
        setText(textNew);
    }

    const checkphoto = async () => {
        setDone(false);
        setProcess(true);
        const file = document.getElementById("file").files[0];
        if (!file) return;

        const lang = document.getElementById("langs").value;

        await recognize(file, lang, updateProgress);
        setResult();
    };

    useEffect(() => {}, [text]);

    return (
        <section className="check">
            <form className="form">
                <div>
                    <h2>Chek your product</h2>
                </div>
                <input
                    name="chek"
                    type="text"
                    placeholder="Enter ingridients"
                />
                <button type={"submit"}>CHECK</button>

                <select id="langs">
                    <option value="rus" selected>
                        Русский
                    </option>
                    <option value="eng">English</option>
                </select>
                <input type="file" id="file" />
                <div id="log">
                    {!done ? (
                        process ? (
                            <>
                                <span>{status}</span>
                                <progress
                                    id="progress"
                                    max={1}
                                    value={progress}
                                />
                            </>
                        ) : null
                    ) : (
                        <span>{text}</span>
                    )}
                </div>
                <button type="button" id="start" onClick={checkphoto}>
                    Начать обработку
                </button>
            </form>
        </section>
    );
}

export default CheckIngridients;