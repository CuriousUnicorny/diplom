import React, {useEffect} from "react";
import {Link, Route, Routes, useNavigate} from 'react-router-dom'
import {useState} from "react";
import '../styles/Style.css';
import {useSelector} from "react-redux";
import {useActions} from "../hooks/useActions";
import { Database } from "firebase/database";
import validator from 'validator'
import {database} from "../utils/firebase-config";

function AdminPage() {

    const initialState ={
        name: "",
        link: "",
        desc: ""
    }

    const setName = (name) =>{
        initialState.name = name;
    }
    const setLink = (link) =>{
        initialState.link = link;
    }
    const setDesc = (desc) =>{
        initialState.desc = desc;
    }
    
    const [nameError, setNameError] = useState('');
    const [linkError, setLinkError] = useState('');
    const [descError, setDescError] = useState('');
    const [formValid, setFormValid] = useState('');
    const {user, loading, error} = useSelector((state) => state.user);
    const[state, setState] = useState(initialState);
    const[data, setData] = useState({});

    const{name, link, desc} = state; 
    const {addIgrAction} = useActions();

    const add = async (e) =>{
        e.preventDefault();
        try{
            await addIgrAction(name, link, desc);
            console.log(initialState)
        }catch(error){
            console.log(error.message);
        }

    }

    const nameHandler=(e)=>{
        setName(e.target.value)
        const re = /^[a-zA-Z\-]+$/;
        if(!re.test(String(e.target.value).toLowerCase())){
            setNameError('Only characters A-Z, a-z and \'-\' are  acceptable.')
        }else{
            setNameError("")
        }
    }
    const linkHandler=(e)=>{
        setLink(e.target.value)
        if (!validator.isURL(e.target.value)) {
            setLinkError('URL not valid')
        } else{
            setLinkError(); //? 
        }
    }
    const descHandler=(e)=>{
        setDesc(e.target.value)
        const re = /^[a-zA-Z\.\,]+$/;
        if(!re.test(String(e.target.value).toLowerCase())){
            setDescError('Only characters A-Z, a-z, \'.\' and \',\' are  acceptable.')
        }else{
            setDescError(); //? 
        }
    }
    useEffect(()=>{
        console.log(user)
    if (nameError || linkError || descError){
        setFormValid(false)
    }else{
        setFormValid(true)
    }},[nameError, linkError, descError, user, loading])
    


    return (
        <section className="addIngr">
            <form className="form">
                <div>
                    <h2>Add new ingridient</h2>
                </div>
                <input name="name" type="text" placeholder="Enter name" onChange={nameHandler}/>
                <input name="link" type="text" placeholder="Past link" onChange={linkHandler}/>
                <input name="desc" type="text" placeholder="Enter description" onChange={descHandler}/>
                <button type={"submit"}>ADD</button>
            </form>
        </section>
    )
}

export default AdminPage;