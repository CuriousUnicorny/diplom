import React from "react";
import {Link} from "react-router-dom";
import '../styles/Style.css';
function Load(){
    return(
        <section className="load">
            <div className="load-wrapper">
                <div className="title">
                    <h1>Raiink</h1>
                </div>
                <div className="sign">
                    <Link to='/authorization'><button>Log in</button></Link>
                </div>
                <div className="guest">
                    <Link to='/checkingridients'><button>Continue as guest</button></Link>
                </div>
                <div className="admin">
                    <Link to='/adminPage'><button>add</button></Link>
                </div>
            </div>
        </section>
    )
}

export default Load;