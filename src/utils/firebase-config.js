import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
    apiKey: "AIzaSyBpwHMTitbvCMC5OE07G0pPZ1tsc70X8vc",
    authDomain: "diplom-612fa.firebaseapp.com",
    databaseURL: "https://diplom-612fa-default-rtdb.firebaseio.com",
    projectId: "diplom-612fa",
    storageBucket: "diplom-612fa.appspot.com",
    messagingSenderId: "191469821585",
    appId: "1:191469821585:web:748739cbfd6661866fa930",
    measurementId: "G-B5T2FW6EFH"
  };
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const auth = getAuth(app);
export const database = getDatabase(app);
